package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

//import java.util.Comparator;

public class CarComparator implements Comparator {

	@Override
	public int compare(Object arg0, Object arg1) {
		Car car0 = (Car) arg0;
		Car car1 = (Car) arg1;
		if(!car0.getCarBrand().equals(car1.getCarBrand()))
		return car0.getCarBrand().compareTo(car1.getCarBrand());
	else {
		StringBuilder sbCar0 = new StringBuilder(car0.getCarBrand()+car0.getCarModel()+car0.getCarModelOption());
		StringBuilder sbCar1 = new StringBuilder(car1.getCarBrand()+car1.getCarModel()+car1.getCarModelOption());
		return sbCar0.toString().compareTo(sbCar1.toString());
	}	
	}
	public int compare(Car car1, Car car2) {
//		if(!car0.getCarBrand().equals(car1.getCarBrand()))
//			return car0.getCarBrand().compareTo(car1.getCarBrand());
//		else {
//			StringBuilder sbCar0 = new StringBuilder(car0.getCarBrand()+car0.getCarModel()+car0.getCarModelOption());
//			StringBuilder sbCar1 = new StringBuilder(car1.getCarBrand()+car1.getCarModel()+car1.getCarModelOption());
//			return sbCar0.toString().compareTo(sbCar1.toString());
//		}	
		String infoCar1 = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String infoCar2 = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		return infoCar1.compareTo(infoCar2);
	}
	
}
