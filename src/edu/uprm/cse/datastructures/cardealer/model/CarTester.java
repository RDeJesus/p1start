package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarTester {
	public static void main(String[] args) {
		CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		Iterator<Car> it = list.iterator();
		Car car0 = new Car(0, "Toyota", "Corolla", "LE", 0);
		Car car1 = new Car(1,"Jeep","Wrangler","",0);
		Car car2 = new Car(2,"Subaru","Impresa","Sedan",0);
		Car car3 = new Car(3,"Dogde","Ram","",0);
		
		
		list.add(car0);
		System.out.println(list.last());
		list.add(car1);
		System.out.println(list.last());
		list.add(car2);
		list.add(car2);		
		list.add(car2);
		list.add(car2);
		System.out.println(list.last());
		list.add(car3);
		System.out.println(list.last());
		System.out.println(list.size());
		
//		System.out.println(list.remove(car2));
//		System.out.println(list.remove(car3));
//		System.out.println(list.remove(car0));
//		System.out.println(list.remove(car1));
		//list.remove(car2);
		System.out.println(list.get(2));
		
		System.out.println(list.size());
//		System.out.println(list.remove(2));
//		System.out.println(list.removeAll(car2));
//		System.out.println(list.contains(car2));
		System.out.println(list.size());

		System.out.println(it.hasNext());
	}
}
