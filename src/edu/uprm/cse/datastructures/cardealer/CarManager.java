
package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager{

	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCustomers() {
		return cList.toArray();
	}
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id")int id) {
		if(!cList.remove(cList.get(id))) {
			throw new NotFoundException(new JsonError("Error","Car"+id+"not found"));
		}
		return Response.status(200).build();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id")int id){
		if(cList.checkCarId(id)) {
			return cList.get(id);
		}
		else {
			throw new NotFoundException(new JsonError("Error", "Car"+id+"not found"));
		}
	}
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		if(cList.checkCarId((int)car.getCarId())) {
			cList.update(car);
			return Response.status(Response.Status.OK).build();
		}
		else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		cList.add(car);
		return Response.status(201).build();
	}
}