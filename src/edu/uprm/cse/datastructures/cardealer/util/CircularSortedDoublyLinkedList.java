package edu.uprm.cse.datastructures.cardealer.util;


import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E>, Iterator<E>{

	// CircularSortedDoublyLinkedList instance variables  
	private Node<E> header;
	int currentSize;
	private CarComparator comparator;
	private int position;
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(E element, Node<E> next, Node<E> prev) {
			this.element = element;
			this.next = next;
			this.prev = prev;
		}

		public Node() {
			this.element = null;
			this.next = null;
			this.prev = null;
		}



		public E getElement() {
			return  this.element;
		}


		public Node<E> getNext() {
			return next;
		}


		public void setNext(Node<E> next) {
			this.next = next;
		}


		public Node<E> getPrev() {
			return prev;
		}


		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}


		public void setElement(E element) {
			this.element = element;
		}

		public void clean(){
			this.element = null;
			this.next = null;
			this.prev = null;
		}

	}

	public CircularSortedDoublyLinkedList(CarComparator comp) {
		header = new Node<E>(null,header,header);
		comparator = comp;
		currentSize = 0;
		position = 0;
	}


	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedList<E>(new CarComparator());
	}

	@Override
	public boolean add(E e) {
		if(this.isEmpty()) {
			Node<E> newNode = new Node(e,this.header,this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}

		Node<E> temp = header.getNext();
		Node<E> newNode;

		//Joins the last with newNode
		while(temp != header) {
			if(comparator.compare(e,temp.getElement()) <= 0){ // ATTTTACCKKK HEREEEE
				newNode = new Node<>(e,temp,temp.getPrev());
				Node<E> bef = newNode.getPrev();
				bef.setNext(newNode);
				temp.setPrev(newNode);
				currentSize++;
				return true;
			}
			temp = temp.getNext();
		}
		return false;

	}

	@Override
	public int size() {
		return this.currentSize;
	}


	@Override
	public boolean remove(E e) {
		if(!( e instanceof Car) ) return false;

		Node<E> temp = header.getNext();

		while(temp != header) {
			if(comparator.compare(e, temp.getElement()) == 0) {
				Node<E> bef = temp.getPrev();
				Node<E> aft = temp.getNext();
				bef.setNext(aft);
				aft.setPrev(bef);
				temp.clean();
				currentSize--;
				return true;
			}
			temp = temp.getNext();	
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(isEmpty() || index < 0) {
			return false;
		}

		Node<E> temp = this.header.getNext();
		for(int i = 0;i<this.size();i++,temp = temp.getNext()) {
			if(i== index)
				break;
		}
		return remove(temp.getElement());

	}

	@Override
	public int removeAll(E e) {
		if(isEmpty() || !contains(e))
			return 0;

		int result = 0;

		while(contains(e)) {
			remove(e);
			result++;
		}

		return result;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {		
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if(isEmpty() || index < 0) {
			return null;
		}

		Node<E> temp = this.header.getNext();
		for(int i = 0;i<this.size();i++,temp = temp.getNext()) {
			if(i== index)
				break;
		}
		return temp.getElement();
	}

	@Override
	public void clear() {


	}

	@Override
	public boolean contains(E e) {
		if(this.isEmpty() || e ==null)
			return false;

		Node<E> temp = this.header.getNext();
		for(int i = 0;i<this.size();i++) {
			if(comparator.compare(temp.getElement(), (Car)e) == 0 && temp.getElement().equals((Car)e))
				return true;
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public int firstIndex(E e) {
		if(!this.contains(e))
			return -1;
		Node<E> temp = this.header.getNext();
		for(int i = 0;i<this.size();i++,temp = temp.getNext()) {
			if(temp.getElement().equals(e))
				return i;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		if(!this.contains(e))
			return -1;
		Node<E> temp = this.header.getPrev();
		for(int i = this.size()-1;i>=0;i--,temp = temp.getPrev()) {
			if(temp.getElement().equals(e))
				return i;
		}
		return 0;
	}


	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return this.get(position) != null;
	}


	@Override
	public E next() {
		if (this.hasNext())
			return this.get(position++);
		else
			return null;
	}

	public E[] toArray(){

		Node<E> temp = header.getNext();
		int i = 0;
		E[] arr = (E[]) new Object[currentSize];	

		while(temp!=this.header){
			arr[i] = temp.getElement();
			i++;
			temp = temp.getNext();
		}
		return arr;

	}
	public boolean checkCarId(int e){
		Node<E> temp = this.header.getNext();
		while( temp != header){		
			Car etr = (Car) temp.getElement();
			if(etr.getCarId() == e){
				return true;
			}	
			temp = temp.getNext();
		}	
		return false;		
	}

	public boolean update(E e){
		Node<E> temp = this.header.getNext();
		Car del = (Car) temp.getElement();
		Car update = (Car) e;
		while(temp != header){	
			if(del.getCarId() == update.getCarId()) {
				this.remove(temp.getElement());
				this.add(e);
				return true;
			}
			else {
				temp = temp.getNext();
				del = (Car) temp.getElement();
			}
		}
		return false;
	}

}
